import sys 
   
class Graph(): 
   
    def __init__(self, vertices): 
        self.V = vertices 
        self.graph = [[0 for column in range(vertices)]  
                    for row in range(vertices)] 
   
    def printSolution(self, dist): 
        print ("El camino mas corto es: ") 
        for node in range(self.V): 
            print (node, "a", dist[node]) 
   
 
    def minDistance(self, dist, sptSet): 
   
        # Initilaize minimum distance for next node 
        min = sys.maxsize 
   
        
        for v in range(self.V): 
            if dist[v] < min and sptSet[v] == False: 
                min = dist[v] 
                min_index = v 
   
        return min_index 
   
    # Funtion that implements Dijkstra's single source  
    # shortest path algorithm for a graph represented  
    # using adjacency matrix representation 
    def dijkstra(self, src): 
   
        dist = [sys.maxsize] * self.V 
        dist[src] = 0
        sptSet = [False] * self.V 
   
        for cout in range(self.V): 
   
           
            u = self.minDistance(dist, sptSet) 
   
            sptSet[u] = True
   
           
            for v in range(self.V): 
                if self.graph[u][v] > 0 and \
                    sptSet[v] == False and \
                    dist[v] > dist[u] + self.graph[u][v]: 
                    dist[v] = dist[u] + self.graph[u][v] 
   
        self.printSolution(dist) 
   
class Graph1:  
  
    def __init__(self, vertices):  
        self.V = vertices # No. of vertices  
        self.graph = []  
  
    # function to add an edge to graph  
    def addEdge(self, u, v, w):  
        self.graph.append([u, v, w])  
          
    # utility function used to print the solution  
    def printArr(self, dist):  
        print("El camino es")  
        for i in range(self.V):  
            print("{0} a {1}".format(i, dist[i]))  
      
    # The main function that finds shortest distances from src to  
    # all other vertices using Bellman-Ford algorithm. The function  
    # also detects negative weight cycle  
    def BellmanFord(self, src):  
  
        # Step 1: Initialize distances from src to all other vertices  
        # as INFINITE  w
        dist = [float("Inf")] * self.V  
        dist[src] = 0
  
  
        # Step 2: Relax all edges |V| - 1 times. A simple shortest  
        # path from src to any other vertex can have at-most |V| - 1  
        # edges  
        for _ in range(self.V - 1):  
            # Update dist value and parent index of the adjacent vertices of  
            # the picked vertex. Consider only those vertices which are still in  
            # queue  
            for u, v, w in self.graph:  
                if dist[u] != float("Inf") and dist[u] + w < dist[v]:  
                        dist[v] = dist[u] + w  
  
        # Step 3: check for negative-weight cycles. The above step  
        # guarantees shortest distances if graph doesn't contain  
        # negative weight cycle. If we get a shorter path, then there  
        # is a cycle.  
  
        for u, v, w in self.graph:  
                if dist[u] != float("Inf") and dist[u] + w < dist[v]:  
                        print("Graph contains negative weight cycle") 
                        return
                          
        # print all distance  
        self.printArr(dist)  
  
g1 = Graph1(5)  
g1.addEdge(0, 1, -1)  
g1.addEdge(0, 2, 4)  
g1.addEdge(1, 2, 3)  
g1.addEdge(1, 3, 2)  
g1.addEdge(1, 4, 2)  
g1.addEdge(3, 2, 5)  
g1.addEdge(3, 1, 1)  
g1.addEdge(4, 3, -3)  
  
# Print the solution  


g = Graph(9) 
g.graph = [[0, 4, 0, 0, 0, 0, 0, 8, 0], 
        [4, 0, 8, 0, 0, 0, 0, 11, 0], 
        [0, 8, 0, 7, 0, 4, 0, 0, 2], 
        [0, 0, 7, 0, 9, 14, 0, 0, 0], 
        [0, 0, 0, 9, 0, 10, 0, 0, 0], 
        [0, 0, 4, 14, 10, 0, 2, 0, 0], 
        [0, 0, 0, 0, 0, 2, 0, 1, 6], 
        [8, 11, 0, 0, 0, 0, 1, 0, 7], 
        [0, 0, 2, 0, 0, 0, 6, 7, 0] 
        ]; 
  
 # Floyd Warshall Algorithm in python


# The number of vertices
nV = 4

INF = 999


# Algorithm implementation
def floyd_warshall(G):
    distance = list(map(lambda i: list(map(lambda j: j, i)), G))

    # Adding vertices individually
    for k in range(nV):
        for i in range(nV):
            for j in range(nV):
                distance[i][j] = min(distance[i][j], distance[i][k] + distance[k][j])
    print_solution(distance)


# Printing the solution
def print_solution(distance):
    print("El camino es")
    for i in range(nV):
        for j in range(nV):
            if(distance[i][j] == INF):
                print("INF", end=" ")
            else:
                print(distance[i][j], end="  ")
        print(" ")


G = [[0, 3, INF, 5],
         [2, 0, INF, 4],
         [INF, 1, 0, INF],
         [INF, INF, 2, 0]]




class Corrector:
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups, solo numero enteros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups, solo numeros entero mayores a 0")
                    e=1
        return r


opcion = 0
c = Corrector()
while(opcion!=4):
    print("========== MENU ==========")
    print("Digite 1 para Dijkstra")
    print("Digite 2 para Floyd Warshall")
    print("Digite 3 para Ford Bellman")
    print("Digite 4 para ***SALIR***")
    opcion = c.correcion()
    
    if(opcion == 1):
        g.dijkstra(0);
    elif(opcion == 2):
        floyd_warshall(G)
    elif(opcion == 3):
        g1.BellmanFord(0)
    elif(opcion == 4):
        print("Gracias")
    elif(opcion >4):
        print("esa opcion no exite")
    
    
    


'''
Created on 18 dic. 2020

@author: Cesar
'''
